# vimrc

This repository contains my Vim editor configuration file `.vimrc` that I have
crafted over the years.

## Installing

**Note**: Before you could download and use it, you need to have various
prerequisites installed for this to work correctly.

### Prerequisites

To be updated.

### How to install

I have an URL-shortened link to the `.vimrc` file in this repo under
[bit.do/vimrcragav](https://bit.do/vimrcragav). Hence, getting this on your
machine is as simple as running the following command.

```bash
wget bit.do/vimrcragav -O ~/.vimrc
```

Alternatively, you could clone this repository to a location on your `${HOME}`
directory and create a symbolic link at `~/.vimrc` to this file, as shown below.

```bash
cd ${HOME}
git clone https://gitlab.com/n.ragav/vimrc.git
ln -s ${HOME}/vimrc/.vimrc ${HOME}/.vimrc
```

Now start your vim editor by running `vim`, and have fun.
